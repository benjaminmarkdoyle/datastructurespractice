class Heap {
    constructor() {
        this.data = [];
    }

    rootNode() {
        return this.data[0];
    }

    lastNode() {
        return data[(this.data.length) - 1];
    }

    leftChildIndex(index) {
        return (index * 2) + 1;
    }

    rightChildIndex(index) {
        return (index * 2) + 2;
    }

    parentIndex(index) {
        return (index - 2) / 2;
    }

    hasGreaterChild(index) {
        if (this.data[this.rightChildIndex(index)]) {
            if (this.data[this.rightChildIndex(index)] > this.data[index]) {
                return true;
            }
        }
        if (this.data[this.leftChildIndex(index)]) {
            if (this.data[this.leftChildIndex(index)] > this.data[index]) {
                return true; 
            }
        }
    }

    calculateLargerChildIndex(index) {
        if (!this.data[this.rightChildIndex(index)]) {
            return this.leftChildIndex(index);
        }

        if (this.data[this.rightChildIndex(index)] > this.data[this.leftChildIndex(index)]) {
            return this.rightChildIndex(index);
        } else {
            return this.leftChildIndex(index);
        }
    }

    insert(value) {
        this.data.push(value);

        var newNodeIndex = this.data.length + 1;

        while (newNodeIndex > 0 && this.data[newNodeIndex] > this.data[this.parentIndex(newNodeIndex)]) {
            let parent = this.data[this.parentIndex(newNodeIndex)];
            this.data[this.parentIndex(newNodeIndex)] = this.data[newNodeIndex];
            this.data[newNodeIndex] = parent;
            // this.data[this.parentIndex(newNodeIndex)], this.data[newNodeIndex] = this.data[newNodeIndex], this.data[this.parentIndex(newNodeIndex)];
            newNodeIndex = parentIndex(newNodeIndex);
        }

        console.log(this.data);
    }

    delete() {
        this.data[0] = this.data.pop();

        var trickleNodeIndex = 0;

        while (this.hasGreaterChild(trickleNodeIndex)) {
            var largerChildIndex = this.calculateLargerChildIndex(trickleNodeIndex);

            // this.data[trickleNodeIndex], this.data[largerChildIndex] = this.data[largerChildIndex], this.data[trickleNodeIndex];

            let trickleNode = this.data[trickleNodeIndex];

            this.data[trickleNodeIndex] = this.data[largerChildIndex];
            this.data[largerChildIndex] = trickleNode;

            trickleNodeIndex = largerChildIndex;
        }

        console.log("deleting root...");
        console.log(this.data);
    }
}

var heap = new Heap();

heap.insert(10);
heap.insert(15);
heap.insert(85);
heap.insert(2);
heap.insert(20);
heap.insert(21);

heap.delete();

heap.insert(8);

heap.delete();

// var root = heap.rootNode();

// console.log(root);


// var array = [10, 20];
// console.log(array);

// array[0], array[1] = array[1], array[0];

// console.log(array);