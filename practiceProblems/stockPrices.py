def maxProfit(prices):

    buy = prices[0] #current price we buy at 
    sell = prices[1] #current price we sell at 

    minBuy = buy #minimum price we can buy at, overall 
    

    for i in range(1, len(prices)):

        #[8, 5, 12, 9, 19, 1]
        
        profit = sell - buy #calculate current profit

        if prices[i] - minBuy > profit: #if the current price - minBuy yields a higher profit, set buy to current minBuy 
            buy = minBuy 

        if prices[i] - buy > profit: #if the current price yields a higher profit, set sell to current price 
            sell = prices[i]

        if prices[i] < minBuy: #if current price is lower than minBuy, set minBuy to current price 
            minBuy = prices[i]

    profit = sell - buy

    print(buy)
    print(sell)
    print(profit)

maxProfit([8, 19, 12, 9, 5, 1])
print('------------------')
print('------------------')
maxProfit([21, 20, 12, 10, 6, 3])