#Given an array of positive integers and a targetSum, return the indices of the two integers that add up to the targetSum. 
#There will be exactly two integers that add up to the targetSum.
#Return the indices in an array in any order

def twoSum(array, targetSum):
    valueDict = {array[i]: i for i in range(len(array))}

    for i in range(len(array)):
        if targetSum - array[i] in valueDict:
            if not valueDict[targetSum - array[i]] == i:
                return [i, valueDict[targetSum - array[i]]]

#Big O Analysis:

    #This code has O(N) time complexity. 
        #Creating valueDict runs in O(N), iterating over each element in the array 
        #The next loop also runs in O(N). Using the dictionary results in O(1) lookup time. 

    #The space complexity is O(N) as well. As the array grows, so will the size of valueDict. 
