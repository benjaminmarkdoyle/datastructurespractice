class Node:
    data = None
    left = None
    right = None

    #Recursive pre-order traversal: 
    def preOrder(self, node):
        if node == None: 
            return

        print(node.data)
        self.preOrder(node.left)
        self.preOrder(node.right)

    #Recursive in-order traversal
    def inOrder(self, node):
        if node == None:
            return 
        self.inOrder(node.left)
        print(node.data)
        self.inOrder(node.right)

    def __init__(self, value):
        self.data = value

    def swapChildren(self, node):
        if node == None:
            return

        #only swap if there are two children available to swap 
        if node.left and node.right:
            left = node.left
            node.left = node.right
            node.right = left

        #recursively call to continue down the tree 
        self.swapChildren(node.left)
        self.swapChildren(node.right)

rootNode = Node(10)
rootNode.right = Node(15)
rootNode.left = Node(5)
rootNode.right.right = Node(20)
rootNode.right.left = Node(12)
rootNode.left.right = Node(7)
rootNode.left.left = Node(3)
rootNode.left.left.right = Node(4)

rootNode.inOrder(rootNode)

print("\n")
rootNode.swapChildren(rootNode)
print("\n")
rootNode.inOrder(rootNode)
print('\n')
print(rootNode.data)
