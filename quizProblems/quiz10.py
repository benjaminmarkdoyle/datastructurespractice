def bst_max_min(node, minValue, maxValue):
    # minValue = 1000000000
    # maxValue = 0

    if node.wordcount < minValue:
        minValue = node.wordcount
    if node.wordcount > maxValue:
        maxValue = node.wordcount 

    if node.leftChild:
        return bst_max_min(node.leftChild, minValue, maxValue)
    if node.rightChild:
        return bst_max_min(node.rightChild, minValue, maxValue)

    
    print(minValue)
    print(maxValue)

    return 

class TreeNode:
    def __init__(self,val,left=None,right=None):
        self.wordcount = val
        self.leftChild = left
        self.rightChild = right

def insert(value, node):
    if value < node.wordcount:

        # If the left child does not exist, we want to insert
        # the value as the left child:
        if node.leftChild is None:
            node.leftChild = TreeNode(value)
        else:
            insert(value, node.leftChild)

    elif value > node.wordcount:

        # If the right child does not exist, we want to insert
        # the value as the right child:
        if node.rightChild is None:
            node.rightChild = TreeNode(value)
        else:
            insert(value, node.rightChild)



def getWordCounts(path_to_text):

    with open(path_to_text, 'r') as file:
        text = file.read().replace('\n', '')
    
    words = []
    singleWord = []

    for char in text:
        if char.isalpha():
            singleWord.append(char.lower())
        else:
            if singleWord:
                words.append("".join(singleWord))
                singleWord = []

    wordsDict = {}

    for word in words: 
        if word in wordsDict:
            wordsDict[word] += 1 
        else:
            wordsDict[word] = 1 

    wordCounts = set()

    for value in wordsDict.values():
        wordCounts.add(value)

    return wordCounts 

wordCounts = getWordCounts('datastructurespractice/quizProblems/NephiCh1Vs8.txt')

firstNode = TreeNode(wordCounts.pop())

while wordCounts:
    insert(wordCounts.pop(), firstNode)

bst_max_min(firstNode, firstNode.wordcount, firstNode.wordcount)