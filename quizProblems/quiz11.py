def getUniqueWords(path_to_text):

    with open(path_to_text, 'r') as file:
        text = file.read().replace('\n', '')
    
    words = []
    singleWord = []

    for char in text:
        if char.isalpha():
            singleWord.append(char.lower())
        else:
            if singleWord:
                words.append("".join(singleWord))
                singleWord = []

    wordsSet = set()

    for word in words: 
        if not word in wordsSet:
            wordsSet.add(word)

    return wordsSet

class TrieNode:
    def __init__(self):
        self.children = {}

class Trie:
    def __init__(self):
        self.root = TrieNode()

    def search(self, word): 
        currentNode = self.root
        for char in word:
            if currentNode.children.get(char):
                currentNode = currentNode.children[char]
            else:
                return None
            return currentNode

    def insert(self, word):
        currentNode = self.root
        for char in word:
            if currentNode.children.get(char):
                currentNode = currentNode.children[char]
            else:
                newNode = TrieNode()
                currentNode.children[char] = newNode 
                currentNode = newNode
                currentNode.children["*"] = None
 
    def autocomplete(self, prefix):
        currentNode = self.search(prefix)
        if not currentNode: 
            return None
        return self.collectAllWords(currentNode)

    def collectAllWords(self,node=None,word="",words =[]):
        currentNode = node or self.root 
        for key, childNode in currentNode.children.items():
            if key == "*":
                words.append(word)
            else: 
                self.collectAllWords(childNode,word+key,words)
        return words

#put unique words from text into Trie
words = getUniqueWords('datastructurespractice/quizProblems/NephiCh1Vs8.txt')

myTrie = Trie()

for word in words:
    myTrie.insert(word)
# print(myTrie.collectAllWords())
print(myTrie.autocomplete("famil"))