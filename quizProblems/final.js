 /*** IS537 Template - Linked List Delete Duplicates ***/
 class LinkedList {
    head = null;
    size = 0;

    constructor(value) {
        if (value != null) {
            this.insertNode(value);
        }
    }

    insertNode(value) {
        if (this.head === null) {
            this.head = new LinkNode(value);
                
        } else {
            var temp = this.head;
            this.head = new LinkNode(value);
            this.head.next = temp;
        }
        this.size++
    }
}
        
class LinkNode {
    nextPointer = null;
    data;

    constructor(value) {
        this.data = value;
    }
}

linkedList = new LinkedList();
linkedList.insertNode(2);
linkedList.insertNode(4);
linkedList.insertNode(6);
linkedList.insertNode(8);
linkedList.insertNode(16);
linkedList.insertNode(42);
linkedList.insertNode(23);
linkedList.insertNode(16);
linkedList.insertNode(15);
linkedList.insertNode(8);
linkedList.insertNode(4);
console.log("Size Before: " + linkedList.size);

removeLLDuplicates(linkedList);

var studentName = "Benjamin Doyle";
console.log("Size After: " + linkedList.size);
console.log(studentName);

/*******************************************/
/*********** Place Your Code Here **********/
/**    to find and delete duplicate data  **/
function removeLLDuplicates(llist) {

    //keep track of visited nodes with a Set(). More efficient than an array 
    //because it has a hash-table implementation. 
    var visitedNodes = new Set();

    if (llist.head) {
        let currentNode = llist.head;
        visitedNodes.add(currentNode.data);

        while (currentNode.next != null) {
            //if we have a duplicate node 
            if (visitedNodes.has(currentNode.next.data)) {
                currentNode.next = currentNode.next.next;
                llist.size -= 1;
            } else {
                currentNode = currentNode.next;
                visitedNodes.add(currentNode.data);
            }
        }
    }
}



/*******************************************/