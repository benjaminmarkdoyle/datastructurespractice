class Graph(object):
    def __init__(self, nodes, init_graph):
        self.nodes = nodes
        self.graph = self.construct_graph(nodes, init_graph)
       
    def construct_graph(self, nodes, init_graph):
        graph = {}
        for node in nodes:
            graph[node] = {}
       
        graph.update(init_graph)
       
        for node, edges in graph.items():
            for adjacent_node, value in edges.items():
                if graph[adjacent_node].get(node, False) == False:
                    graph[adjacent_node][node] = value
                   
        return graph
   
    def get_nodes(self):
        return self.nodes
   
    def get_outgoing_edges(self, node):
        connections = []
        for out_node in self.nodes:
            if self.graph[node].get(out_node, False) != False:
                connections.append(out_node)
        return connections
   
    def value(self, node1, node2):
        return self.graph[node1][node2]

def shortest_path(graph, start_node):
    unvisited_nodes = list(graph.get_nodes())

    shortest_path = {}

    previous_nodes = {}

    max_value = 1000000000
    for node in unvisited_nodes:
        shortest_path[node] = max_value
    shortest_path[start_node] = 0
   
    while unvisited_nodes:
        current_min_node = None
        for node in unvisited_nodes: # Iterate over the nodes
            if current_min_node == None:
                current_min_node = node
            elif shortest_path[node] < shortest_path[current_min_node]:
                current_min_node = node
               
        neighbors = graph.get_outgoing_edges(current_min_node)
        for neighbor in neighbors:
            tentative_value = shortest_path[current_min_node] + graph.value(current_min_node, neighbor)
            if tentative_value < shortest_path[neighbor]:
                shortest_path[neighbor] = tentative_value
                previous_nodes[neighbor] = current_min_node

        unvisited_nodes.remove(current_min_node)
   
    return shortest_path


nodes = ["Coalville", "Kamas", "Snyderville", "Oakley", "Park City", "Francis"]

init_graph = {}
for node in nodes:
    init_graph[node] = {}
   
init_graph["Coalville"]["Kamas"] = 16
init_graph["Coalville"]["Snyderville"] = 32
init_graph["Kamas"]["Snyderville"] = 13
init_graph["Kamas"]["Oakley"] = 22
init_graph["Snyderville"]["Park City"] = 8
init_graph["Park City"]["Oakley"] = 40
init_graph["Oakley"]["Francis"] = 12
init_graph["Park City"]["Francis"] = 36

graph = Graph(nodes, init_graph)

shortest_path = shortest_path(graph, "Park City")

print(shortest_path)