class Node: 
    data = None
    left = None
    right = None

    #Recursive pre-order traversal: 
    def preOrder(self, node):
        if node == None: 
            return

        print(node.data)
        self.preOrder(node.left)
        self.preOrder(node.right)

    #Recursive in-order traversal
    def inOrder(self, node):
        if node == None:
            return 
        self.inOrder(node.left)
        print(node.data)
        self.inOrder(node.right)

    #Recursive post-order traversal
    def inOrder(self, node):
        if node == None:
            return 
        self.postOrder(node.left)
        self.postOrder(node.right)        
        print(node.data)
