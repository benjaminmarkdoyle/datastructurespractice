#here is my avl file

class Node:
    def __init__(self, data):
        self.value = data
        self.left = None
        self.right = None
        self.height = 1

class avlTree:
    def insertNode(self, root, key):
        if not root:
            return Node(key)
        elif key < root.key:
            root.left = self.insertNode(root.left, key)
        else:
            root.right = self.insertNode(root.right, key)

        # root.height = 1 + max(self.ge)

